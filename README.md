# Chronos

Chronos is part of the [Framaspace ecosystem](https://framagit.org/framasoft/framaspace/).

Chronos is the service which handles the cron run of the Nextcloud instances.

## Dependencies

On Debian:
```bash
apt install python3-logbook python3-requests python3-psycopg2
```

## Installation

```bash
cd /opt
git clone https://framagit.org/framasoft/framaspace/chronos
cp chronos/chronos.service /etc/systemd/system/
```
Edit `/etc/systemd/system/chronos.service` if your web server user is not `www-data`

## Configuration

The configuration needs to be in `/etc/chronos.yml`.

For security, restrict the access to this file (replace `www-data` with the user of your web server):
```bash
chown www-data: /etc/chronos.yml
chmod 600 /etc/chronos.yml
```

Minimal configuration:

```yml
db:
  name: db_name
  user: db_user
  password: db_password
threads: 10
delay: 5
batch_size: 10
stale:
  delay: 60
  strategy: unlock_and_notify
```

Explanation:
- `db` contains the database configuration
- `threads` is the number of concurrent cron jobs (default: 10)
- `delay`: how many minutes between two cron jobs for the same domain? (default: 10)
- `batch_size`: how many domains to retrieve from the database at the same time (default: 10)
- `stale.delay`: the number of minutes after which a lock is considered stale (default: 60)
- `stale.strategy`: can be `none` (disable stale lock detection), `unlock`, `notify`, `unlock_and_notify` (default: `unlock`)

If you want to be notified of failures with mails, add:
```
mail:
  from: chronos@example.org
  host: 127.0.0.1
  port: 25
  ssl: False
  starttls: False
  auth:
    login: corge
    password: grault
  addresses:
    - foobar@example.org
    - bazqux@example.org
```

Only `from` and `addresses` are mandatory.
Default `host` and `port` are `127.0.0.1` and `25`, `ssl` and `starttls` are `False` and `auth` has no default value.

Please note that the mail authentification and SSL/STARTTLS connection have not been tested!
Report any issue on <https://framagit.org/framasoft/framaspace/chronos/-/issues/new>.

If you want to be notified of failures with gotify, add:
```
gotify:
  url: https://gotify.example.org
  tokens:
    - foobar
    - bazqux
```

## Start

```
systemctl daemon-reload
systemctl enable --now chronos
```

## LICENSE

© Framasoft 2022, licensed under the terms of the GNU GPLv3.
See [LICENSE](LICENSE) file
