#!/usr/bin/python3

"""Chronos, a daemon to run cron jobs for Nextcloud

Debian dependencies:
  python3-logbook
  python3-psycopg2
  python3-requests

To lint the file, install python3-pycodestyle, pylint, mypy, then:
  python /usr/lib/python3/dist-packages/pycodestyle.py chronos.py
  pylint --rcfile .pylintrc chronos.py
  mypy chronos.py
"""


import sys
import ssl
import json
import time
import smtplib
import datetime
import subprocess
from os import uname
from pprint import pformat
from multiprocessing import Pool
import logbook  # type: ignore
import psycopg2
import requests
import yaml


DEFAULT_STALE_STRATEGY = 'unlock'

config: dict = {}


def notify_domain(domain: str, stdout: str, stderr: str):
    """ Notify when cron fails

    Keyword arguments:
    domain -- string, domain concerned
    stdout -- string
    stderr -- string
    """
    hostname = uname().nodename
    if 'gotify' in config and 'tokens' in config['gotify']:
        logger.debug('Notify of failure with gotify')
        payload = {'title': f'Cron failed for domain {domain} on '
                            f'{hostname}',
                   'message': f'stdout: {stdout}'
                              f'stderr: {stderr}',
                   'priority': 9}
        notify_gotify(payload)
    if 'mail' in config and 'addresses' in config['mail']:
        logger.debug('Notify of failure by mail')
        msg = f"""\
Subject: Cron failed for domain {domain} on {hostname}

stdout: {stdout}
stderr: {stderr}"""
        notify_by_mail(msg)


def notify_stale_lock(domain: str, last_cron: datetime.datetime):
    """ Notify when a domain’s lock is staled

    Keyword arguments:
    domain    -- string, domain concerned
    last_cron -- datetime, date of the last cron run
    """
    extra_msg = 'Please go to the database to remove the lock.\n' \
                'You will be notified every hour until the stale ' \
                'lock is removed'
    priority = 9
    if config['stale']['strategy'] in ['unlock', 'unlock_and_notify']:
        extra_msg = 'Nothing to do: the lock will be automatically removed.'
        priority = 0

    if 'gotify' in config and 'tokens' in config['gotify']:
        logger.debug('Notify of stale lock with gotify')
        payload = {'title': f'Stale lock for domain {domain}',
                   'message': 'The last cron job was run on '
                              f'{last_cron.strftime("%A %d %B at %Hh%M")}.\n'
                              f'{extra_msg}',
                   'priority': priority}
        notify_gotify(payload)
    if 'mail' in config and 'addresses' in config['mail']:
        logger.debug('Notify of stale lock by mail')
        msg = f"""\
Subject: Stale lock for domain {domain}

The last cron job was run on {last_cron.strftime('%A %d %B at %Hh%M')}.
{extra_msg}"""
        notify_by_mail(msg)


def notify_error(err: str):
    """ Notify when error occurred

    Keyword argument:
    err -- string, the error
    """
    hostname = uname().nodename
    if 'gotify' in config and 'tokens' in config['gotify']:
        logger.debug('Notify of error with gotify')
        payload = {'title': f'Error in Chronos on {hostname}!',
                   'message': f'{err}',
                   'priority': 9}
        notify_gotify(payload)
    if 'mail' in config and 'addresses' in config['mail']:
        logger.debug('Notify of error by mail')
        msg = f"""\
Subject: Error in Chronos on {hostname}!

{err}"""
        notify_by_mail(msg)


def notify_gotify(payload: dict):
    """ Notify with gotify

    Keyword argument:
    payload: dict, payload suitable to use with gotify
    """
    headers = {'accept': 'application/json',
               'content-type': 'application/json'}

    for token in config['gotify']['tokens']:
        logger.debug(f'Sending gotify message to {config["gotify"]["url"]}')
        logger.debug(pformat(payload, indent=2))
        try:
            req = requests.post(f'{config["gotify"]["url"]}/message',
                                params={'token': token},
                                headers=headers,
                                timeout=60,
                                data=json.dumps(payload))
        except requests.exceptions.RequestException as req_err:
            logger.warning('[GOTIFY] Requests error: '
                           f'{pformat(req_err, indent=2)}')
        else:
            if req.status_code != 200:
                logger.error(f'HTTPError: {req.status_code} {req.reason}')


def notify_by_mail(msg: str):
    """ Notify by mail

    Keyword argument:
    msg -- string, the mail to send (including subject)
    """
    if 'host' not in config['mail']:
        config['mail']['host'] = '127.0.0.1'
    if 'port' not in config['mail']:
        config['mail']['port'] = 25
    if 'ssl' not in config['mail']:
        config['mail']['ssl'] = False
    if 'starttls' not in config['mail']:
        config['mail']['starttls'] = False

    if config['mail']['ssl']:
        logger.debug('Mail notification: SSL')
        context = ssl.create_default_context()
        smtp = smtplib.SMTP_SSL(host=config['mail']['host'],
                                port=config['mail']['port'],
                                context=context)
    else:
        smtp = smtplib.SMTP(host=config['mail']['host'],  # type: ignore
                            port=config['mail']['port'])
        if config['mail']['starttls']:
            logger.debug('Mail notification: STARTTLS')
            context = ssl.create_default_context()
            smtp.starttls(context=context)

    if 'auth' in config['mail']:
        if 'login' not in config['mail']['auth'] \
           or 'password' not in config['mail']['auth']:
            logger.warning('Mail credentials are incomplete. '
                           'No mail authentication will be done')
            return

        logger.debug('Mail notification: authentification')
        smtp.login(config['mail']['auth']['login'],
                   config['mail']['auth']['password'])

    for address in config['mail']['addresses']:
        logger.debug(f'Sending mail to {address}')
        logger.debug(msg)
        smtp.sendmail(config['mail']['from'], address, msg)


def run_cron(domain: str):
    """ Run cron.php for a nextcloud instance

    Keyword argument:
    domain -- string, the domain of the instance concerned
    """
    logger.debug(f'Running cron for {domain}')

    res = subprocess.run('/usr/bin/php /var/www/nc-instances/'
                         f'{domain}.{config["domain"]}/cron.php',
                         env={'NC_INSTANCE': domain},
                         shell=True,
                         check=False,
                         capture_output=True,
                         text=True)

    logger.debug(f'Return code: {res.returncode}')
    logger.debug(f'Stdout: {res.stdout}')
    logger.debug(f'Stderr: {res.stderr}')

    return {'domain': domain, 'result': res}


def main() -> int:  # pylint: disable-msg=R0912,R0914,R0915
    """ Check for stale locks, starts cron runs, reports error
    """

    # Connect to DB
    logger.debug(f'Connecting to {config["db"]["name"]} DB')
    conn = psycopg2.connect(f'dbname={config["db"]["name"]} '
                            f'user={config["db"]["user"]} '
                            f'password={config["db"]["password"]}')
    cur = conn.cursor()

    # Create the table if needed
    create_table_query = """\
CREATE TABLE IF NOT EXISTS domains(
    domain              TEXT PRIMARY KEY,
    lock                BOOLEAN DEFAULT False,
    last_cron           TIMESTAMP DEFAULT NOW(),
    last_failure        TIMESTAMP,
    last_failure_stdout TEXT,
    last_failure_stderr TEXT
);
ALTER TABLE domains
ADD COLUMN IF NOT EXISTS last_stale_detection TIMESTAMP;"""
    try:
        cur.execute(create_table_query)
        conn.commit()
    except psycopg2.errors.ReadOnlySqlTransaction:  # type: ignore
        conn.rollback()
        psycopg_err = 'Psycopg2 error at create_table_query: ' \
                      'ReadOnlySqlTransaction'
        logger.error(psycopg_err)
        notify_error(psycopg_err)
    except Exception as psycopg_err:  # pylint: disable-msg=W0703
        logger.error(psycopg_err)
        notify_error(pformat(psycopg_err))

    detect_stale_locks_query = f"""\
UPDATE domains
SET last_stale_detection = NOW()
WHERE lock = True
AND last_cron < NOW() - INTERVAL'{config['stale']['delay']} minutes'
AND (last_stale_detection < NOW() - INTERVAL'60 minutes'
OR last_stale_detection IS NULL)
RETURNING domain, last_cron"""

    update_returning_query = f"""\
UPDATE domains
SET lock = True
WHERE domain IN (
    SELECT domain
    FROM domains
    WHERE last_cron < NOW() - INTERVAL'{config['delay']} minutes'
    AND lock = False
    LIMIT {config['batch_size']}
)
RETURNING domain;"""

    while True:
        logger.debug('Begin of the loop')

        # Detect stale locks
        if config['stale']['strategy'] != 'none':
            stale_locks = []
            try:
                cur.execute(detect_stale_locks_query)
                conn.commit()
                stale_locks = [item for item in cur.fetchall()]  # pylint: disable-msg=R1721  # noqa
            except psycopg2.errors.ReadOnlySqlTransaction:  # type: ignore
                conn.rollback()
                pg_error = 'Psycopg2 error at unlock stale locks: ' \
                           'ReadOnlySqlTransaction'
                logger.error(pg_error)
                notify_error(pg_error)

            if len(stale_locks) > 0:
                logger.warning(f'{len(stale_locks)} domain(s) with stale lock')
                stale_domains = []

                for item in stale_locks:
                    if config['stale']['strategy'] in \
                       ['notify', 'unlock_and_notify']:
                        notify_stale_lock(item[0], item[1])
                    if config['stale']['strategy'] in \
                       ['unlock', 'unlock_and_notify']:
                        stale_domains.append(item[0])

                if config['stale']['strategy'] in \
                   ['unlock', 'unlock_and_notify']:
                    try:
                        cur.execute("""\
UPDATE domains
SET lock = False
WHERE domain = ANY(%(stale_domains)s)""", {'stale_domains': stale_domains})
                        conn.commit()
                    except psycopg2.errors.ReadOnlySqlTransaction:  # type: ignore  # pylint: disable-msg=C0301  # noqa
                        conn.rollback()
                        pg_error = 'Psycopg2 error at unlock stale locks: ' \
                                   'ReadOnlySqlTransaction'
                        logger.error(pg_error)
                        notify_error(pg_error)

        # Get domains which need to run cron jobs
        domains = []
        try:
            cur.execute(update_returning_query)
            conn.commit()
            domains = [item for item, in cur.fetchall()]
        except psycopg2.errors.ReadOnlySqlTransaction:  # type: ignore
            conn.rollback()
            pg_error = 'Psycopg2 error at update_returning_query: ' \
                       'ReadOnlySqlTransaction'
            logger.error(pg_error)
            notify_error(pg_error)

        # Run cron jobs
        if len(domains) > 0:
            logger.info(f'{len(domains)} domains to process with '
                        f'{config["threads"]} threads')
            with Pool(config['threads']) as pool:
                results = pool.map(run_cron, list(domains))
                for ret in results:
                    domain = ret['domain']
                    if ret['result'].returncode == 0:
                        logger.debug(f'Run successful for {domain}')
                        try:
                            cur.execute("""\
UPDATE domains
SET lock = False,
    last_cron = NOW()
WHERE domain = %(domain)s""", {'domain': domain})
                            conn.commit()
                        except psycopg2.errors.ReadOnlySqlTransaction:  # type: ignore  # pylint: disable-msg=C0301  # noqa
                            conn.rollback()
                            pg_error = 'Psycopg2 error at update_success: ' \
                                       'ReadOnlySqlTransaction'
                            logger.error(pg_error)
                            notify_error(pg_error)
                    else:
                        logger.warning(f'Run failure for {domain}')
                        logger.debug(
                            'stdout: '
                            f'{pformat(ret["result"].stdout, indent=2)}')
                        logger.debug(
                            'stderr: '
                            f'{pformat(ret["result"].stderr, indent=2)}')
                        try:
                            cur.execute("""\
UPDATE domains
SET lock = False,
    last_cron = NOW(),
    last_failure = NOW(),
    last_failure_stdout = %(out)s,
    last_failure_stderr = %(err)s
WHERE domain = %(domain)s""", {'out': ret['result'].stdout,
                               'err': ret['result'].stderr,
                               'domain': domain})
                            conn.commit()
                        except psycopg2.errors.ReadOnlySqlTransaction:  # type: ignore  # pylint: disable-msg=C0301  # noqa
                            conn.rollback()
                            pg_error = 'Psycopg2 error at update_error: ' \
                                       'ReadOnlySqlTransaction'
                            logger.error(pg_error)
                            notify_error(pg_error)

                        notify_domain(domain,
                                      ret['result'].stdout,
                                      ret['result'].stderr)
        else:
            logger.info('No domain to process. Sleeping 60 seconds')
            time.sleep(60)


if __name__ == '__main__':
    # Init logger
    logger = logbook.Logger('chronos', level=logbook.INFO)
    log_handler = logbook.StreamHandler(
            sys.stdout,
            format_string='{record.level_name}: {record.message}',
            bubble=True)
    log_handler.push_application()

    # Read config
    with open('/etc/chronos.yml', 'r', encoding='utf-8') as file:
        config = yaml.safe_load(file)
        file.close()

    # Prevent configuration problems
    error = 0  # pylint: disable-msg=C0103
    if 'db' not in config:
        error += 1
    else:
        if 'name' not in config['db']:
            error += 2
        if 'user' not in config['db']:
            error += 4
        if 'password' not in config['db']:
            error += 10
    if 'domain' not in config:
        error += 20
    if 'mail' in config and 'from' not in config['mail']:
        error += 100

    if 'threads' not in config:
        config['threads'] = 10
    if 'batch_size' not in config:
        config['batch_size'] = 10
    if 'delay' not in config:
        config['delay'] = 10
    if 'stale' not in config:
        config['stale'] = {'delay': 60, 'strategy': DEFAULT_STALE_STRATEGY}
    else:
        if 'delay' not in config['stale']:
            config['stale']['delay'] = 60
        if 'strategy' not in config['stale']:
            config['stale']['strategy'] = DEFAULT_STALE_STRATEGY
        if config['stale']['strategy'] not in \
           ['none', 'notify', 'unlock', 'unlock_and_notify']:
            logger.warning('Unknown stale strategy '
                           f'"{config["stale"]["strategy"]}", using '
                           f'{DEFAULT_STALE_STRATEGY}')
            config['stale']['strategy'] = DEFAULT_STALE_STRATEGY

    if 'debug' in config and config['debug']:
        logger.level = logbook.DEBUG

    if error > 0:
        if 'db' in config and 'password' in config['db']:
            config['db']['passwd'] = 'REDACTED'
        if 'mail' in config and 'auth' in config['mail'] \
           and 'password' in config['mail']['auth']:
            config['mail']['auth']['password'] = 'REDACTED'

        logger.critical(
                'Error: Some configuration parameters are missing! '
                f'Config:\n{pformat(config, indent=2, width=1)}')
        sys.exit(error)

    sys.exit(main())
